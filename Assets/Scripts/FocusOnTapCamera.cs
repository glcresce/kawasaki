using UnityEngine;
using System.Collections;
using Vuforia;

public class FocusOnTapCamera : MonoBehaviour {
	
	private static bool isContinuousFocusAvailable = false;
	private static bool isStationaryTouchHappening = false;
	
	// Use this for initialization
	void Start () {
		VuforiaBehaviour.Instance.RegisterVuforiaStartedCallback(OnVuforiaStarted);
		VuforiaBehaviour.Instance.RegisterOnPauseCallback(OnPaused);
		isContinuousFocusAvailable = CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
	}
	
	private void OnVuforiaStarted(){
		Focus ();
	}
	
	private void OnPaused(bool paused){
		if (!paused) {
			Debug.Log("App resumed.");
			Focus();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(!isContinuousFocusAvailable) {
			if(Input.touchCount > 0) {
				TouchPhase phase = Input.GetTouch (0).phase;
				switch(phase){
				case TouchPhase.Began:
				case TouchPhase.Stationary:
					isStationaryTouchHappening = true;
					break;
				case TouchPhase.Moved:
				case TouchPhase.Canceled:
					isStationaryTouchHappening = false;
					break;
				case TouchPhase.Ended:
					if(isStationaryTouchHappening) {
						isStationaryTouchHappening = false;
						CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
					}
					break;
				}
			}
		}
	}
	
	
	
	public static void Focus(){
		isContinuousFocusAvailable = CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
		if (isContinuousFocusAvailable) 
			Debug.Log("Continuous Focus Available and set.");
		else
			CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
		/*if(!isContinuousFocusAvailable)
			CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_NORMAL);*/
	}
}
