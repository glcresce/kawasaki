﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;

public class IconsGenerator : EditorWindow {

	public string path = "Assets/Resources/Icons";
	public Texture2D icon;

	[MenuItem ("Experenti/Icons/Generate icons automatically...")]
	static void GenerateIcons() {
		IconsGenerator window = (IconsGenerator)EditorWindow.GetWindow (typeof (IconsGenerator));
	}

	void OnGUI() {
		icon = (Texture2D)EditorGUILayout.ObjectField("Icon:", icon, typeof(Texture2D), false);
		if(GUILayout.Button ("Generate all icons and associate them to PlayerSettings")) {
			Directory.CreateDirectory(path);
			string[] androidIcons = GenerateIconsForSizes(PlayerSettings.GetIconSizesForTargetGroup(BuildTargetGroup.Android), "Android");
			string[] iosIcons = GenerateIconsForSizes(PlayerSettings.GetIconSizesForTargetGroup(BuildTargetGroup.iOS), "iOS");
			AssetDatabase.Refresh();
			ChangeImportSettingsForTextures(androidIcons);
			ChangeImportSettingsForTextures(iosIcons);
			AssociateTexturesToPlatformIcons(androidIcons, BuildTargetGroup.Android);
			AssociateTexturesToPlatformIcons(iosIcons, BuildTargetGroup.iOS);
		}
	}

	private string[] GenerateIconsForSizes(int[] sizes, string folder) {
		Directory.CreateDirectory (path + "/" + folder);
		string [] icons = new string[sizes.Length];
		for(int i = 0; i < sizes.Length; i++) {
			int size = sizes[i];
			Texture2D newIcon = new Texture2D(icon.width, icon.height);
			for(int x = 0; x < icon.width; x++)
				for(int y = 0; y < icon.height; y++)
					newIcon.SetPixel(x, y, icon.GetPixel(x,y));
			newIcon.Apply ();
			TextureScale.Bilinear(newIcon, size, size);
			byte[] bytesIcon = newIcon.EncodeToPNG();
			string completePath = path + "/" + folder + "/" + size + ".png";
			if(bytesIcon != null)
					File.WriteAllBytes(completePath, bytesIcon);
			DestroyImmediate(newIcon);
			icons[i] = completePath;
		}
		return icons;
	}

	private void ChangeImportSettingsForTextures(string[] iconPaths) {
		foreach(string iconPath in iconPaths) {
			TextureImporter textureImporter = AssetImporter.GetAtPath(iconPath) as TextureImporter;
			textureImporter.textureType = TextureImporterType.GUI;
			AssetDatabase.ImportAsset(iconPath);
		}
	}

	private void AssociateTexturesToPlatformIcons(string[] iconsPath, BuildTargetGroup platform) {
		Texture2D[] icons = new Texture2D[iconsPath.Length];
		for(int i = 0; i < iconsPath.Length; i++) {
			string iconPath = iconsPath[i];
			Texture2D icon = (Texture2D)AssetDatabase.LoadAssetAtPath(iconPath, typeof(Texture2D));
			icons[i] = icon;
		}
		PlayerSettings.SetIconsForTargetGroup(platform, icons);
	}
	

}
